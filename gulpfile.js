const $ = require('gulp-load-plugins')();
const babelify = require('babelify');
const browserify = require('browserify');
const buffer = require('vinyl-buffer');
const gulp = require('gulp');
const path = require('path');
const source = require('vinyl-source-stream');

/*
 * Fractal.
 */
const fractal = module.exports = require('@frctl/fractal').create();
const twigAdapter = require('@frctl/twig')();
fractal.components.engine(twigAdapter);
fractal.components.set('ext', '.twig');
fractal.set('project.title', 'Fractal test');
fractal.components.set('path', path.join(__dirname, 'patterns'));
fractal.components.set('label', 'Patterns');
fractal.components.set('default.preview', '@preview');
fractal.docs.set('path', path.join(__dirname, 'docs'));
const mandelbrot = require('@frctl/mandelbrot');
const customTheme = mandelbrot({
    skin: "navy",
    styles: [
        "/themes/mandelbrot/css/navy.css",
    ],
    nav: ['search', 'docs', 'components', 'information']
});
fractal.web.theme(customTheme);
fractal.web.set('static.path', path.join(__dirname, 'assets'));
fractal.web.set('builder.dest', __dirname + '/public');
const logger = fractal.cli.console;

// Browsers to target when prefixing CSS.
const autoprefixerCompatibility = [
    'last 2 versions',
    'ie >= 9',
    'safari >= 8'
];

// Livereload over SSL
const livereloadSettings = {};

function css (file, dest = './public/dist/css') {
    console.log('css');

    const dir = path.resolve(`${__dirname}/../css`);

    return gulp.src(file)
        .pipe($.sourcemaps.init({loadMaps: true}))
        .pipe($.sass()
            .on('error', $.sass.logError))
        .pipe($.autoprefixer({
            overrideBrowserslist: autoprefixerCompatibility
        }))

        // Output CSS file
        .pipe(gulp.dest(dest))

        // Minify with maps and output
        .pipe($.sourcemaps.init())
        .pipe($.cleanCss())
        .pipe($.rename({suffix: '.min'}))
        .pipe($.sourcemaps.write('.'))
        .pipe(gulp.dest(dest))

        // Run livereload
        .pipe($.filter([
            path.join(dir, "/public/**/*.css"),
            "/public/**/*.css"
        ]))
        .pipe($.livereload());
}

// Build the theme CSS file
gulp.task('build-css', gulp.series(() => css('./assets/src/scss/style.scss')));

gulp.task('build-fractal', gulp.series(() => {
    const builder = fractal.web.builder();
    builder.on('progress', (completed, total) => logger.update(`Exported ${completed} of ${total} items`, 'info'));
    builder.on('error', err => logger.error(err.message));
    return builder.build().then(() => {
        logger.success('Fractal build completed!');
    });
}));

gulp.task('build', gulp.series([
    'build-fractal',
    'build-css',
]));
