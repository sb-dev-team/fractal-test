# Pattern library general information

---

## Directory descriptions

There are four main directories in the pattern library: assets, docs, patterns and public.

### Assets

This contains any images that are used as placeholders for the pattern library as well as all the general (i.e non-component specifc) SCSS and Javascript that are used. Things to note regarding the scss directory are:

- There is a fractal directory which is referring to styles which are specific to the pattern library theme itself. These are separated out, so they don't get compiled into the main CSS file.
- The style.scss file also needs the component.scss files from the patterns directory imported into it. For example, `@import "../../../patterns/components/card/card";` Without adding them here, the styles won't get rendered in the pattern library
  and won't be built in the actual CSS file.

### Docs

The docs directory can contain any informational content required for the pattern library. By default, it will just include an index markdown file which will serve as the landing page for the pattern library.

### Patterns

The patterns directory is where all the patterns themselves are added and curated.

- The directory set-up generally follows the same structure as the scss assets, with components and objects.
- Each pattern should consist of the following files:
    - A config yaml file which contains dummy data.
    - A scss file for styling. This also needs to be imported into styles.scss in the `assets/src/scss` directory.
    - A twig file for the HTML structure of the pattern.
    - A README markdown file for any necessary further information about the pattern.
- The patterns directory is generally called 'components' by default in Fractal - and will be referred to as such in all of their documentation. The directory name has been changed to reference 'patterns' in the Fractal gulp settings.
- Upon running the `build-fractal` task, these are the patterns that will get rendered into the library.

### Public

The contents of the public directory are generated when `build-fractal` is run. Any assets directly added into the public directory will be removed as soon as the `build-fractal` task is run, so this should never be used as a working directory.

---

## Gulpfile

### What do the Fractal specific gulp tasks do?

- build-fractal-theme-css gulp task is specifically for any customisations to the pattern library theme (mandelbrot)
- build-fractal-css gulp task is for any styles required to make the pattern library/components work correctly, for example including grid.scss.
- build-fractal is the main task that will actually pull together all of the components in the patterns directory and create the pattern library. However, it does not build the `dist` folder. It is recommended to run the `build` task which also
  runs `build-css` and `build-fractal-css` after the main Fractal build.

---

## Simple guide to adding a new pattern

1. Within the `patterns` directory, duplicate an existing pattern directory (i.e 'card') to get all the necessary files
2. Rename each of the files to match your new component and create your new pattern
3. Make sure the new component is referenced within `assets/src/scss/style.scss`
4. Run `build` (to build both CSS and Fractal)

---

## Using variants

To create a variant of a pattern, the simplest way is to duplicate the twig file and rename it using modifier syntax, i.e `button.twig` would become `button--variation.twig`. Make any necessary modifications in this new file, then rebuild fractal.
For more information on variants, see: https://fractal.build/guide/components/variants.html#creating-variants